close all
clear all
clc

%% Call HyperBB files
% Code to process

%% Makes graphs pretty
for in=1
set(gca, 'defaultAxesFontName', 'Arial');
set(groot, 'defaultAxesFontSize', 16);
set(groot, 'defaultAxesFontWeight', 'b');
set(groot, 'defaultLineLineWidth', 2);
set(groot, 'defaultAxesLineWidth', 2);
set(groot,'defaultFigureColor', 'none')
set(groot, 'defaultAxesColor','w')
set(groot,'defaultLineMarkerSize',15);
set(gca,'color','w')
set(gcf,'color','w')
set(0,'defaultfigurecolor','w')
end

%%
%define the file paths for save in and out
raw_HBB_dc='C:\Users\benlo\Documents\MATLAB\Ocean optics Workshop\BYOB\d_raw\HBB';%where the data is
processed_HBB_dc='C:\Users\benlo\Documents\MATLAB\Ocean optics Workshop\BYOB\d_processed\HBB';%where you want it to be output

time='0930';

cd(raw_HBB_dc) %changes 
addpath(raw_HBB_dc)

% DONT CHANGE: define calfile names
calfile_temp = 'HBB_Cal_Temp_20210106_105500.mat';
calfile_plaque = 'Hbb_Cal_Plaque_20210719_150430.mat';

% CHANGE HERE
hypbb_dark = 'BYOB_HBB_DARK_0930.csv';%dont change, only one dark count
eval(['hypbb_data_filt = ''BYOB_HBB_FIL_' time '.csv'';'])
eval(['hypbb_data_tot = ''BYOB_HBB_TOT_' time '.csv'';'])

%% Inport the data

verbose = true;

% Run HBB import and processing
[ dark, lambdabb ] = import_and_processHBB( hypbb_dark, calfile_plaque, calfile_temp, verbose );
[ data_filt, lambda_f ] = import_and_processHBB( hypbb_data_filt, calfile_plaque, calfile_temp, verbose );
[ data_tot, lambda_t ] = import_and_processHBB( hypbb_data_tot, calfile_plaque, calfile_temp, verbose );

%% Split the single data column title beta into individual column
darkbb = table2array(splitvars(dark(:,5),'beta'));
databb_filt = table2array(splitvars(data_filt(:,5),'beta'));
databb_tot = table2array(splitvars(data_tot(:,5),'beta'));

%% Remove any NaN values as to not ruin statistics
darkbb(any(isnan(darkbb), 2), :) = [];
databb_filt(any(isnan(databb_filt), 2), :) = [];
databb_tot(any(isnan(databb_tot), 2), :) = [];

%% Match vector sizes to smallest file
mint = min(size(databb_filt,1),size(darkbb,1));
mint2 = min(size(databb_tot,1),mint);

darkbb = darkbb(1:mint2,:);
databb_filt = databb_filt(1:mint2,:);
databb_tot = databb_tot(1:mint2,:);

%% do the dark subtraction to get corrected spectra
hyper_bb_corr_filt = databb_filt - darkbb;
hyper_bb_corr_tot = databb_tot - darkbb;

%% statistics (time averaged)
meanhypbb_filt = mean(hyper_bb_corr_filt,1);
stdhypbb_filt = std(hyper_bb_corr_filt);

meanhypbb_tot = mean(hyper_bb_corr_tot,1);
stdhypbb_tot = std(hyper_bb_corr_tot);

%% Plot the data for inspection
plot(lambdabb',meanhypbb_filt)
ylabel('b_{b,135^{o}} (m^{-1})')
xlabel('\lambda (nm)')
eval(['title(''HBB at ' time ''')'])
x_filt = lambdabb';
y_filt = meanhypbb_filt;
sd_filt = stdhypbb_filt;
patch([x_filt fliplr(x_filt)], [y_filt+sd_filt fliplr(y_filt-sd_filt) ], [.5 .5 .5], 'FaceAlpha',0.5, 'EdgeColor','none')
hold on
plot(lambdabb',meanhypbb_tot)
x_tot = lambdabb';
y_tot = meanhypbb_tot;
sd_tot = stdhypbb_filt;
patch([x_tot fliplr(x_tot)], [y_tot+sd_tot fliplr(y_tot-sd_tot) ], [.5 .5 .5], 'FaceAlpha',0.5, 'EdgeColor','none')
hold off

%% concatonate data to combine statistics
% Out put is 3 columns
% 1: wavelength [=] nm
% 2: time averaged bb135 [=] m^-1
% 3: time std. dev bb135 
eval(['HBB_DATA_' time ' = [lambdabb, meanhypbb_filt'', stdhypbb_filt'', meanhypbb_tot'', stdhypbb_tot''];'])
HBB_head=char('lambdabb','mean_fil','SD_fil','mean_tot','SD_tot');

%%
cd(processed_HBB_dc)
%eval(['save(''BYOB_HBB_processed_' time ''',''HBB_DATA_' time ''',''HBB_head'');'])

