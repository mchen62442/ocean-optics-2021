close all
clear all
clc

% Code by luka modified by charlotte and ben

%% Call HyperBB files
% Code to process

%% Makes graphs pretty
for in=1
set(gca, 'defaultAxesFontName', 'Arial');
set(groot, 'defaultAxesFontSize', 16);
set(groot, 'defaultAxesFontWeight', 'b');
set(groot, 'defaultLineLineWidth', 2);
set(groot, 'defaultAxesLineWidth', 2);
set(groot,'defaultFigureColor', 'none')
set(groot, 'defaultAxesColor','w')
set(groot,'defaultLineMarkerSize',15);
set(gca,'color','w')
set(gcf,'color','w')
set(0,'defaultfigurecolor','w')
end

%%
%define the file paths for save in and out
raw_HBB_dc=('/Users/charlotte.begouen/Desktop/Optic_Class_Lab/Project_data/d_raw/HBB/');%where the data is


% DONT CHANGE: define calfile names
calfile_temp = 'HBB_Cal_Temp_20210106_105500.mat';
calfile_plaque = 'Hbb_Cal_Plaque_20210719_150430.mat';

% CHANGE HERE
hypbb_dark = '/Users/charlotte.begouen/Desktop/Optic_Class_Lab/Project_data/d_raw/HBB/BYOB_HBB_DARK_0930.csv';%dont change, only one dark count

 % Get all filtered files
l = dir([raw_HBB_dc filesep 'BYOB_HBB_FIL_*.csv']);
n_files = length(l);
filtered_n = cell(n_files,1);
for i = 1:n_files
 filtered_n{i} = [l(i).folder filesep l(i).name];
end

% Get all TSW files 
l = dir([raw_HBB_dc filesep 'BYOB_HBB_TOT_*.csv']);
n_files = length(l);
total_n = cell(n_files,1);
for i = 1:n_files
total_n{i} = [l(i).folder filesep l(i).name];
end

%% Inport the data - dark
verbose = true;
[ dark, lambdabb ] = import_and_processHBB( hypbb_dark, calfile_plaque, calfile_temp, verbose );
% Split the single data column title beta into individual column
darkbb = table2array(splitvars(dark(:,5),'beta'));
% Remove any NaN values as to not ruin statistics
darkbb(any(isnan(darkbb), 2), :) = [];


%% Run HBB Import on each file 
for i = 1:size(filtered_n,1)
% Run HBB import and processing
[ data_filt, lambda_f ] = import_and_processHBB( filtered_n{i}, calfile_plaque, calfile_temp, verbose );
[ data_tot, lambda_t ] = import_and_processHBB( total_n{i}, calfile_plaque, calfile_temp, verbose );

%Split the single data column title beta into individual column

databb_filt = table2array(splitvars(data_filt(:,5),'beta'));
databb_tot = table2array(splitvars(data_tot(:,5),'beta'));

% Remove any NaN values as to not ruin statistics
databb_filt(any(isnan(databb_filt), 2), :) = [];
databb_tot(any(isnan(databb_tot), 2), :) = [];

% Match vector sizes to smallest file
mint = min(size(databb_filt,1),size(darkbb,1));
mint2 = min(size(databb_tot,1),mint);

darkbb = darkbb(1:mint2,:);
databb_filt = databb_filt(1:mint2,:);
databb_tot = databb_tot(1:mint2,:);

% do the dark subtraction to get corrected spectra
hyper_bb_corr_filt = databb_filt - darkbb;
hyper_bb_corr_tot = databb_tot - darkbb;

% statistics (time averaged)
medhypbb_filt = median(hyper_bb_corr_filt,1);
stdhypbb_filt = std(hyper_bb_corr_filt);

medhypbb_tot = median(hyper_bb_corr_tot,1);
stdhypbb_tot = std(hyper_bb_corr_tot);

% Plot the data for inspection
%
% plot(lambdabb',medhypbb_filt)
% ylabel('b_{b,135^{o}} (m^{-1})')
% xlabel('\lambda (nm)')
% x_filt = lambdabb';
% y_filt = meanhypbb_filt;
% sd_filt = stdhypbb_filt;
% patch([x_filt fliplr(x_filt)], [y_filt+sd_filt fliplr(y_filt-sd_filt) ], [.5 .5 .5], 'FaceAlpha',0.5, 'EdgeColor','none')
% hold on
% plot(lambdabb',hyper_bb_corr_tot)
% x_tot = lambdabb';
% y_tot = meanhypbb_tot;
% sd_tot = stdhypbb_filt;
% patch([x_tot fliplr(x_tot)], [y_tot+sd_tot fliplr(y_tot-sd_tot) ], [.5 .5 .5], 'FaceAlpha',0.5, 'EdgeColor','none')
% hold off

figure(11)
 txt =['time stamp #' num2str(i)];
plot(lambdabb', medhypbb_tot,'DisplayName',txt)
 hold on 
legend show

Hbb_p = array2table(medhypbb_tot-medhypbb_filt,'VariableName',string(lambdabb'));

writetable(Hbb_p, ['preprocessed_' l(i).name])
end

%% concatonate data to combine statistics
% Out put is 3 columns
% 1: wavelength [=] nm
% 2: time averaged bb135 [=] m^-1
% 3: time std. dev bb135 
% eval(['HBB_DATA_' time ' = [lambdabb, meanhypbb_filt'', stdhypbb_filt'', meanhypbb_tot'', stdhypbb_tot''];'])
% HBB_head=char('lambdabb','mean_fil','SD_fil','mean_tot','SD_tot');
% 
% %%
% cd(processed_HBB_dc)
%eval(['save(''BYOB_HBB_processed_' time ''',''HBB_DATA_' time ''',''HBB_head'');'])

