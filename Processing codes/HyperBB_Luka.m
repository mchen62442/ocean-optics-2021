
%% Call HyperBB files
% Code to process
set(gca, 'defaultAxesFontName', 'Arial');
set(groot, 'defaultAxesFontSize', 16);
set(groot, 'defaultAxesFontWeight', 'b');
set(groot, 'defaultLineLineWidth', 2);
set(groot, 'defaultAxesLineWidth', 2);
set(groot,'defaultFigureColor', 'none')
set(groot, 'defaultAxesColor','w')
set(groot,'defaultLineMarkerSize',15);
set(gca,'color','w')
set(gcf,'color','w')
set(0,'defaultfigurecolor','w')

% DONT CHANGE: define calfile names
calfile_temp = 'HBB_Cal_Temp_20210106_105500.mat';
calfile_plaque = 'Hbb_Cal_Plaque_20210719_150430.mat';

% CHANGE HERE
hypbb_dark = 'DARK_HyperBB8002_20210804_205631.csv';
hypbb_data = 'HyperBB8002_20210804_202526.csv';

verbose = true;

% Run HBB import and processing
[ dark, lambdabb ] = import_and_processHBB( hypbb_dark, calfile_plaque, calfile_temp, verbose );
[ data, lambda ] = import_and_processHBB( hypbb_data, calfile_plaque, calfile_temp, verbose );

% Split the single data column title beta into individual column
darkbb = table2array(splitvars(dark(:,5),'beta'));
databb = table2array(splitvars(data(:,5),'beta'));

% Remove any NaN values as to not ruin statistics
darkbb(any(isnan(darkbb), 2), :) = [];
databb(any(isnan(databb), 2), :) = [];

% Match vector sizes to smallest file
mint = min(size(databb,1),size(darkbb,1));

darkbb = darkbb(1:mint,:);
databb = databb(1:mint,:);

% do the dark subtraction to get corrected spectra
hyper_bb_corr = databb - darkbb;

% statistics (time averaged)
meanhypbb = mean(hyper_bb_corr,1);
stdhypbb = std(hyper_bb_corr);

plot(lambdabb',meanhypbb)
ylabel('b_{b,135^{o}} (m^{-1})')
xlabel('\lambda (nm)')
x = lambdabb';
y = meanhypbb;
sd = stdhypbb;
patch([x fliplr(x)], [y+sd fliplr(y-sd) ], [.5 .5 .5], 'FaceAlpha',0.5, 'EdgeColor','none')


% concatonate data to combine statistics
% Out put is 3 columns
% 1: wavelength [=] nm
% 2: time averaged bb135 [=] m^-1
% 3: time std. dev bb135 
HYPBB_DATA = [lambdabb, meanhypbb', stdhypbb'];

%writematrix(HYPBB_DATA,'HYPBB_Data_.csv') 


% if you want spectral slope please ask me but the real data most likely
% won't be fit by a simple single exponential


%7.383728519749190e+05