close all
clear all
clc

%%

%you really just have to set your cv and load in data here

%define where you data is and where you want it to go
raw_acs_dc='C:\Users\benlo\Documents\MATLAB\Ocean optics Workshop\BYOB\d_raw\ACS';%where the data is
processed_acs_dc='C:\Users\benlo\Documents\MATLAB\Ocean optics Workshop\BYOB\d_processed\ACS';%where you want it to be output

time='1330';

cd(raw_acs_dc) %changes 
addpath(raw_acs_dc)

%load in data
eval(['[filt_data_a , filt_lambda_a, filt_lambda_c ] = importInlininoACScsv(''BYOB_ACS_a_FIL_' time '.csv'');']) %filtrate
eval(['[filt_data_c , x, y ] = importInlininoACScsv(''BYOB_ACS_c_FIL_' time '.csv'');']) %filtrate
eval(['[tot_data_a , tot_lambda_a, tot_lambda_c ] = importInlininoACScsv(''BYOB_ACS_a_TOT_' time '.csv'');']) %total
eval(['[tot_data_c , x, y] = importInlininoACScsv(''BYOB_ACS_c_TOT_' time '.csv'');']) %total
clear x y


%%
%define variables. a is absorption, c is attenuation
%different wavelengths associated with a and c side so must define
%separately

%data for filtered
fa=filt_data_a.a; %defines the a_filter data as fa
fc=filt_data_c.c; %defines the c_filter data as fc
fa_wave = NaN(size(filt_lambda_a,2),1);%creates a nan filled wavelength_a
fc_wave = NaN(size(filt_lambda_c,2),1);%creates a nan filled wavelength_c

for i = 1: size(filt_lambda_a,2)
    fa_wave(i) = str2num(filt_lambda_a{1,i});
end

for i = 1: size(filt_lambda_c,2)
    fc_wave(i) = str2num(filt_lambda_c{1,i});
end 


figure(1)
plot(fa_wave,fa,'.')
hold on
plot(fc_wave,fc,'.')
title('acs filtered open')
hold off


%%
%unsmooth data
fa_acs_unsmoothed = unsmoothACS(fa, fa_wave);
fc_acs_unsmoothed = unsmoothACS(fc, fc_wave);

figure(2)
plot(fa_wave,fa_acs_unsmoothed,'.')
hold on
plot(fc_wave,fc_acs_unsmoothed,'.')
title('acs filtered unsmoothed')
hold off

%%
%take median
fa_med=median(fa_acs_unsmoothed);
fc_med=median(fc_acs_unsmoothed);

figure(3)
plot(fa_wave,fa_med,'.')
hold on
plot(fc_wave,fc_med,'.')
title('acs filtered median')
hold off


%%


%must interpolate
fa=fa_med;
fc=transpose(interp1(fc_wave,fc_med,fa_wave));
fwl=transpose(fa_wave);

figure(4)
plot(fwl,fa,'.')
hold on
plot(fwl,fc,'.')
title('acs filtered wave fix')
hold off

%%
%%temp and salinity correction if measuring relative to blank
%[fa, fc] = TemperatureAndSalinityDependence(fa, fc, fwl)

%%check it
%plot(fwl,fa)
%plot(fwl,fc)

%%
%data for total
ta = tot_data_a.a;
tc=tot_data_c.c;
ta_wave = NaN(size(tot_lambda_a,2),1);
tc_wave = NaN(size(tot_lambda_c,2),1);

for i = 1: size(tot_lambda_a,2)
    ta_wave(i) = str2num(tot_lambda_a{1,i});
end

for i = 1: size(tot_lambda_c,2)
    tc_wave(i) = str2num(tot_lambda_c{1,i});
end 

figure(5)
plot(ta_wave,ta,'.')
hold on
plot(tc_wave,tc,'.')
title('acs total open')
hold off

%%
%unsmooth data
ta_acs_unsmoothed = unsmoothACS(ta, ta_wave);
tc_acs_unsmoothed = unsmoothACS(tc, tc_wave);

figure(6)
plot(ta_wave,ta_acs_unsmoothed,'.')
hold on
plot(tc_wave,tc_acs_unsmoothed,'.')
title('acs total unsmoothed')
hold off

%%
%take median
ta_med=median(ta_acs_unsmoothed);
tc_med=median(tc_acs_unsmoothed);

figure(7)
plot(ta_wave,ta_med,'.')
hold on
plot(tc_wave,tc_med,'.')
title('acs total median')
hold off

%%
%must interpolate
ta=ta_med;
tc=transpose(interp1(tc_wave,tc_med,ta_wave));
twl=transpose(ta_wave);

figure(8)
plot(twl,ta,'.')
hold on
plot(twl,tc,'.')
title('acs total wave fix')
hold off

%% temp and salinity correction if measuring relative to blank
%[ta, tc] = TemperatureAndSalinityDependence(ta, tc, tl)
%%check it
%plot(twl,ta)
%plot(twl,tc)

%%
%Particulate
ap=ta-fa;
cp=tc-fc;
wl=twl;
[ap_corr, cp_corr] = ResidualTemperatureAndScatteringCorrection(ap, cp, wl);

%corrected vs. uncorrected particulate absorption and attenuation
figure(9)
plot(wl,ap_corr,'LineWidth',2)
xlabel('Wavelength (nm)','FontSize',18)
ylabel('coeff (m^{-1})','FontSize',18)
hold on
plot(wl,ap,'LineWidth',2)
plot(wl,cp_corr,'LineWidth',2)
plot(wl,cp,'LineWidth',2)
hold off
ax = gca;
ax.FontSize = 16; 
legend('AP corr','AP','CP corr','CP','FontSize',18)

%%
eval(['ap_' time '=ap_corr;'])
eval(['cp_' time '=cp_corr;'])
eval(['wl_' time '=wl;'])

%%
cd(processed_acs_dc)
eval(['save(''BYOB_ACS_processed_' time '.mat'',''ap_' time ''',''cp_' time ''',''wl_' time ''');'])
