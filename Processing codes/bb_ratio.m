%% Get ACS data to extract Backscattering 

data=load('C:\Users\benlo\Documents\MATLAB\Ocean optics Workshop\Cruise\Inline\wk\ACS94\ACS94_20210804_prod_p.mat');
load('ACS94_lambda_a.mat') ;
load('ACS94_lambda_c.mat') ;

data=data.data;

% Get scattering from ACS data.
b = data.cp -data.ap ;
a= data.ap;
c= data.cp;
figure
plot(lambda_a,b)
title('b 1/m)')
figure
plot(lambda_a,a)
title('a 1/m)')
figure
plot(lambda_c,c)
title('c unitless')
dt = data.data.dt;
b = array2table(b, 'VariableNames',("b_" + lambda_a));
b.dt = dt;


% Get bb from BB3 
bb_data = load('C:\Users\benlo\Documents\MATLAB\Ocean optics Workshop\Cruise\Inline\wk\BB3349\BB3_20210804_prod_p.mat');
bbp = bb_data.data.bbp;
bbp_dt = bb_data.data.dt;
load('BB3_lambda.mat')

% plot BB3
%bbp_dt = datetime(bbp_dt,'ConvertFrom','datenum');
scatter(bbp_dt,bbp(:,1));
bbp= array2table(bbp, 'VariableNames',("bb_"+ bb_lambda));
bbp.dt = bbp_dt;

% Compute bb/b ratio
ratio = outerjoin(bbp,b,'MergeKeys',true,'Keys','dt');
rat = table();
rat.r470 = ratio.bb_470./ratio.("b_468.7");
rat.r532 = ratio.bb_532./ratio.("b_534.6");
rat.r660 = ratio.bb_660./ratio.("b_659.3");

%plot 
scatter(470,rat.r470)
hold on 
scatter(532,rat.r532)
scatter(660,rat.r660)


%% Hyperbb STUFF
