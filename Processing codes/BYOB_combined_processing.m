%% addtion BB3 processing 
% acs numbers of total_a (a_T) and total_b (b_T)
a=[0.0416,0.0159,-0.003];
b=[3.4844,3.3199,2.9609];

%correction spectra
eps = 0.4;
bbp_corr = bb_tscor.*exp(L.*(a+b.*eps)); % with b and epsilon
bbp_corr_w = bb_tscor.*exp(L.*a);        % without 

figure(3)
plot(bbp_corr_w)

% Convert particulate VSF to b_bp
b_b_p=2*pi*1.076*bbp_corr;
b_b_w=2*pi*1.076*bbp_corr_w;

b_b_per = abs((b_b_p-b_b_w)./b_b_w).*100;
%
figure(2)
hold on
plot(lam_flit,b_b_p)
plot(lam_flit,b_b_w)
plot(lam_flit,bb_tscor)
hold off

%
load('AZD_acs_processed.mat')

bl = 17;
grn = 29;
red = 52;

bp = [AZD_b_calibrated(:,bl),AZD_b_calibrated(:,grn),AZD_b_calibrated(:,red)];

b_beta = b_b_p./bp;

figure(4) 
plot(lam_flit,b_beta)

