clear
close all
%% Get ACS data to extract Backscattering 
 load('/Users/charlotte.begouen/Desktop/Optic_Class_Lab/Project_data/processed/BYOB_ACS_processed_0930.mat') ;

 ap = ap_0930;
 cp = cp_0930;

 % Get scattering from ACS data.
 bp = cp - ap ;
 bp = array2table(bp, 'VariableNames',("b_" + wl_0930));% Get absorption for correction of path length( later for ratio)
 ap = array2table(ap, 'VariableNames',("a_" + wl_0930));

 
 figure(1)
  plot(wl_0930,bp{:,:})
hold on 
  plot(wl_0930,ap{:,:})
 legend('Scattering Total','Absorption Total');


clear ap_0930
clear cp_0930
% Need a(DI) to get particulate 
 load('/Users/charlotte.begouen/Desktop/Optic_Class_Lab/Project_data/processed/BYOB_ACS_processed_DI_0930.mat') ;
 ad = ap_0930;
 cd = cp_0930;
 
 bd = cd - ad ;
 bd = array2table(bd, 'VariableNames',("b_" + wl_0930));
 ad = array2table(ad, 'VariableNames',("a_" + wl_0930));

 figure(2)
  plot(wl_0930,bd{:,:})
hold on 
  plot(wl_0930,ad{:,:})
 legend('Scattering dissolved','Absorption Dissolved');

%%
% % Get bb from BB3 
% bb_data = load('/Users/charlotte.begouen/Desktop/Optic_Class_Lab/CRUISE_INLINE/OO2021_Inline/wk/BB3349/BB3_20210804_prod_p.mat');
% load('/Users/charlotte.begouen/Desktop/Optic_Class_Lab/Cruise_data_process/BB3_lambda.mat');
% bbp = bb_data.data.bbp;
% bbp_dt = bb_data.data.dt;
% bbp_lambda = bb_lambda;
% 
% % plot BB3
% bbp= array2table(bbp, 'VariableNames',("bb_"+ bbp_lambda));
% bbp.dt = bbp_dt;
% scatter(bbp_dt,bbp.bb_470);
% 
% % Compute bb/b ratio
% ratio = outerjoin(bbp,b,'MergeKeys',true,'Keys','dt');
% rat = table();
% rat.r470 = ratio.bb_470./((ratio.("b_468.7")+ratio.("b_479.2"))/2); % Take mean of 2 closest wavelengths
% rat.r532 = ratio.bb_532./((ratio.("b_534.6")+ratio.("b_529.9"))/2);
% rat.r660 = ratio.bb_660./((ratio.("b_659.3")+ratio.("b_663.9"))/2);
% 
% %plot 
% plot(470,rat.r470)
% hold on 
% plot(532,rat.r532)
% plot(660,rat.r660)
% 
% plot(bbp_lambda,rat{:,:})
% title('bb_p/b from BB3 Inline');
% ylabel('bb_p/b');
% xlabel('Wavelength (nm)');
% %bbp_dt = datetime(bbp_dt,'ConvertFrom','datenum');

%% Hyperbb STUFF
hy_bb = load('/Users/charlotte.begouen/Desktop/Optic_Class_Lab/Project_data/processed/preprocessed_BYOB_HBB_TOT_0930.csv');
hy_bb_DI = load('/Users/charlotte.begouen/Desktop/Optic_Class_Lab/Project_data/processed/preprocessed_DI_.csv');
hy_bb_DI = [hy_bb_DI(1,:) ;median(hy_bb_DI(2:end,:))];
lambdabb = hy_bb(1,:);

% Get beta of SeaWater from Zang
addpath('/Users/charlotte.begouen/Desktop/Optic_Class_Lab/lab4') %path where the Zhang computation is 
[betasw,beta90sw,bsw]= betasw_ZHH2009(lambdabb,19.7,135,0); % Extract betasw - the beta of seawater at T=19.7 and S = 0;

figure(15)
plot(lambdabb,betasw)
hold on 
plot(lambdabb,hy_bb_DI(2,:))
legend('Beta of Zhang DI','Beta of bucket DI');
hold off

%plot(hy_bb_DI(2,:)-betasw)


% Interpolate acs wavelengths down to the hyper bb ones 
b_interp = interp1(wl_0930,bp{:,:},lambdabb);
a_interp = interp1(wl_0930,ap{:,:},lambdabb); 
bd_interp = interp1(wl_0930,bd{:,:},lambdabb);
ad_interp = interp1(wl_0930,ad{:,:},lambdabb);
bp_interp = b_interp - bd_interp;

figure(16)
plot(lambdabb,bp_interp)
title('Particulate scattering')

% Correction 
anw = a_interp-ad_interp; % Absorption of not water 
k_scat = 3.30; % From H-scat simulations 

clear K_bb
clear B_cor
% First estimation of Kbb 
K_bb = anw + k_scat .* (2 .* pi .*1.08 * (hy_bb(2,:) - betasw));

% First estimation of Bcor 
B_cor = hy_bb(2,:).*exp(0.04*K_bb);

for i = 2:10
% Calculate bb 
K_bb(i,:) = anw + (k_scat .* 2 .* pi .* (B_cor(i-1,:)-betasw));
B_cor(i,:) = hy_bb(2,:).*exp(0.04*K_bb(i,:));
end 

figure(5)
plot(lambdabb,hy_bb(2,:))
hold on
plot(lambdabb,B_cor(3,:))
plot(lambdabb,B_cor(4,:))
title('Iteration of correction of Beta')

% Grab bb data from bucket time
hyper_bb = 1.08*2*pi.*(B_cor(5,:)-betasw);
hyper_bb_unc = 1.08*2*pi.* (hy_bb(2,:)-betasw);

% Compute ratio for each time stamp
hyper_bb = hyper_bb./bp_interp;
hyper_bb_unc = hyper_bb_unc./bp_interp;

m_hyper_bb = hyper_bb;

% Graph taking into account temporal variab.
figure(6)
plot(lambdabb,m_hyper_bb)
 ylabel('bb_p/b_p')
 xlabel('\lambda (nm)')
% x = lambdabb;
% y = m_hyper_bb;
% %sd = sd_hyper_bb;
% %patch([x fliplr(x)], [y+sd fliplr(y-sd) ], [.5 .5 .5], 'FaceAlpha',0.5, 'EdgeColor','none')
% %ylim([0.008 0.014])
% title('Hyper Bb/b');


figure(3)
plot(lambdabb,hyper_bb_unc)
hold on 
plot(lambdabb,hyper_bb)
ylabel('bb_p/b_p')
xlabel('\lambda (nm)')
x = lambdabb;
%y = m_hyper_bb_c;
%sd = sd_hyper_bb_c;
%patch([x fliplr(x)], [y+sd fliplr(y-sd) ], [.5 .5 .5], 'FaceAlpha',0.5, 'EdgeColor','none')
title('Hyper Bb/b Corrected VS Uncorrected');
legend('Uncorrected','Corrected')


figure(4) 
plot(lambdabb,hyper_bb - hyper_bb_unc)
ylabel('bb_p/b_p difference')
xlabel('\lambda (nm)')
title('Correction Impact according to wavelength ');


