close all
clear all
clc


%% download files


raw_BB3_dc='C:\Users\benlo\Documents\MATLAB\Ocean optics Workshop\BYOB\d_raw\BB3';%where the data is
processed_BB3_dc='C:\Users\benlo\Documents\MATLAB\Ocean optics Workshop\BYOB\d_processed\BB3';%where you want it to be output

%items to change
time='0930';
Temp=19.6;
Sal=32;

eval(['auxiliary_' time '=[Temp, Sal];'])

cd(raw_BB3_dc) %changes 
addpath(raw_BB3_dc)


% file paths
dark = ('BYOB_BB3_DARK_0930.csv');
eval(['filt = (''BYOB_BB3_FIL_' time '.csv'');'])
eval(['tot = (''BYOB_BB3_TOT_' time '.csv'');'])

%% import BB3
[dat_dark, lam_dark] = importInlininoBB3(dark);
[dat_flit, lam_flit] = importInlininoBB3(filt);
[dat_tot, lam_tot] = importInlininoBB3(tot);

%% set data to variable
bb_dark = mean(dat_dark.beta);
bb_filt_mean = mean(dat_flit.beta);
bb_tot_mean = mean(dat_tot.beta);

% pathlength from manufacturer (m)
L=0.0391;

%% calibration values
cal_470nm = 8.407E-06;
cal_532nm = 4.624E-06;
cal_650nm = 4.090E-06;

cal = [cal_470nm,cal_532nm,cal_650nm];

%%
% (signal - dark)* calibration factor
% scattering at 124 deg
bb_filt_cal = (bb_filt_mean - bb_dark).*cal;
bb_tot_cal = (bb_tot_mean - bb_dark).*cal;

figure(1)
plot(lam_flit,bb_filt_cal)
hold on
plot(lam_tot,bb_tot_cal)
title('bb3 calibrated')
legend('filter','total')
hold off


%% correct for temp and salinity
[betasw,beta90sw,bsw]= betasw_ZHH2009(lam_flit,Temp,124,Sal);%lambda,temp (c), theta(degrees), salinity (psu), delta

bb_filt_tscor = bb_filt_cal-betasw;%bb_filtered_temperature and salinity corrected
bb_tot_tscor = bb_tot_cal-betasw;%bb_total_temperature and salinity corrected

figure(2)
plot(lam_flit,bb_filt_tscor)
hold on
plot(lam_tot,bb_tot_tscor)
title('bb3 temp and salinity calibrated')
legend('filter','total')
xlabel('wavelenght (nm)')
ylabel('b_b (1/m) ')
hold off

%%
% percent difference in orig and corrected for temp and salinity
bb_filt_per = abs((bb_filt_tscor - bb_filt_cal)./bb_filt_cal)*100;
bb_tot_per = abs((bb_tot_tscor - bb_tot_cal)./bb_tot_cal)*100;

figure(3)
plot(lam_flit,bb_filt_per)
hold on
plot(lam_tot,bb_tot_per)
title('bb3 % change for temp and sal correction')
xlabel('wavelenght (nm)')
ylabel('% change')
legend('filter','total')
hold off

%%
eval(['bb_filt_' time '=bb_filt_tscor;'])
eval(['bb_tot_' time '=bb_tot_tscor;'])


cd(processed_BB3_dc)
%eval(['save(''BYOB_BB3_processed_' time '.mat'',''lam_flit'',''bb_tot_' time ''',''bb_filt_' time ''',''auxiliary_' time ''');'])

