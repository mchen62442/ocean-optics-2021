close all
clear all
clc

%%% Essentialy the same code as the other one but uses medians and loops
%%% through all the files at once

% Written by Sebastian, modified by Charlotte and Ben 
%% download files

raw_BB3_dc='C:\Users\benlo\Documents\MATLAB\Ocean optics Workshop\BYOB\d_raw\BB3';%where the data is
processed_BB3_dc='C:\Users\benlo\Documents\MATLAB\Ocean optics Workshop\BYOB\d_processed\BB3';%where you want it to be output
dark = ('C:\Users\benlo\Documents\MATLAB\Ocean optics Workshop\BYOB\d_raw\BB3\BYOB_BB3_DARK_0930.csv'); 
T_S = readmatrix('C:\Users\benlo\Documents\MATLAB\Ocean optics Workshop\BYOB\d_raw\T_S_reading.xlsx');



% Get all filtered files
l = dir([raw_BB3_dc filesep 'BYOB_BB3_FIL_*.csv']);
n_files = length(l);
filtered_n = cell(n_files,1);
for i = 1:n_files
 filtered_n{i} = [l(i).folder filesep l(i).name];
end

% Get all TSW files 
l = dir([raw_BB3_dc filesep 'BYOB_BB3_TOT_*.csv']);
n_files = length(l);
total_n = cell(n_files,1);
for i = 1:n_files
total_n{i} = [l(i).folder filesep l(i).name];
end


%% import BB3 - dark 
[dat_dark, lam_dark] = importInlininoBB3(dark); % keep
bb_dark = median(dat_dark.beta);

plot(lam_dark,bb_dark);

%% %% calibration values - From Guillaume Email
cal_470nm = 8.407E-06;
cal_532nm = 4.624E-06;
cal_650nm = 4.090E-06;

cal = [cal_470nm,cal_532nm,cal_650nm];

%% correction values 

eps = 0.4; % From litterature - to fix
L = 0.0391; % From manufacturer

%% Loop through and do calculation for each file. 

cd(processed_BB3_dc)

for i = 1:size(filtered_n,1)
Temp=T_S(i,1);
Sal=T_S(i,2);
    
    
[dat_flit, lam_flit] = importInlininoBB3(filtered_n{i});
[dat_tot, lam_tot] = importInlininoBB3(total_n{i});
% plot(lam_flit,dat_flit{:,2:end})

% set data to variable
bb_filt_med = median(dat_flit.beta);
bb_tot_med = median(dat_tot.beta);

% (signal - dark)* calibration factor
% scattering at 124 deg
bb_filt_cal = (bb_filt_med - bb_dark).*cal;
bb_tot_cal = (bb_tot_med - bb_dark).*cal;
%{ 
 figure(1)
 plot(lam_flit,bb_filt_cal)
 hold on
 plot(lam_tot,bb_tot_cal)
 title('bb3 calibrated')
 legend('filter','total')
 hold off
%} 
% correct for temp and salinity
[betasw,beta90sw,bsw]= betasw_ZHH2009(lam_flit,Temp,124,Sal);%lambda,temp (c), theta(degrees), salinity (psu), delta

bb_filt_tscor = bb_filt_cal-betasw;%bb_filtered_temperature and salinity corrected
bb_tot_tscor = bb_tot_cal-betasw;%bb_total_temperature and salinity corrected
%{
 figure(2)
 plot(lam_flit,bb_filt_tscor)
 hold on
 plot(lam_tot,bb_tot_tscor)
 title('bb3 temp and salinity calibrated')
 legend('filter','total')
 xlabel('wavelenght (nm)')
 ylabel('b_b (1/m) ')
 hold off
%}

bb_Part_tscor = bb_tot_tscor - bb_filt_tscor; % Get only particulate part

figure(10)
 txt =['time stamp #' num2str(i)];
plot(lam_tot,bb_Part_tscor,'DisplayName',txt)
 hold on 
legend show

bb_output=[bb_Part_tscor;bb_tot_tscor;bb_filt_tscor];


bb_output_T = array2table(bb_output,'VariableNames', string(lam_dark));

writetable(bb_output_T,['preprocessed_' l(i).name ]);
end 

hold off